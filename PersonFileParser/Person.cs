﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonFileParser
{
    class Person
    {
        public String Firstname { get; set; }
        public String Lastname { get; set; }
        public int Age { get; set; }
    }
}
