﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonFileParser
{
    public enum State
    {
        FIND_DEBUT,
        EVAL_DEBUT,
        FIND_KEY_OR_FIN,
        EVAL_KEY_OR_FIN,
        FIND_VALUE,
        EVAL_VALUE,
        ERROR,
    }

    delegate void OnBeforeEvalItem(Object sender, ParsingArgs args);
    delegate void OnAfterEvalItem(Object sender, ParsingArgs args);

    class ParsingArgs: EventArgs
    {
        public char Item { get; set; }
        public string Token { get; set; }
        public State State { get; set; }
        public int Line { get; set; }
        public int Column { get; set; }
    }

    class PersonParser : IParser<Person>
    {
        public event OnBeforeEvalItem BeforeEvalItem;
        public event OnAfterEvalItem AfterEvalItem;
        private String errorMessage = "";

        public List<Person> Parse(string content)
        {
            List<Person> list = new List<Person>();
            State state = State.FIND_DEBUT ;
            char item;
            String token = "";
            int idxTokenStartLine = 1;
            int idxTokenStartCol = 1;

            int idxLine = 1;
            int idxColumn = 1;
            Person person = new Person();
            String field = "" ;

            content = content + " "; // Enfore error checking at the end

            for (int idx = 0; idx<content.Length; idx++)
            {
                item = content[idx];
                if (null != BeforeEvalItem) BeforeEvalItem(this, new ParsingArgs()
                {
                    Item = item,
                    Token = token,
                    State = state,
                    Line = idxLine,
                    Column = idxColumn
                });
                switch (state)
                {
                    case State.FIND_DEBUT:
                        if (char.IsDigit(item) || char.IsPunctuation(item) || char.IsSymbol(item))
                        {
                            state = State.ERROR;
                            errorMessage = $"Invalid character found at line{idxLine}:{idxColumn}, but looking for keyword 'debut'";
                        }
                        else if (char.IsWhiteSpace(item) || '\r' == item)
                        {
                            state = State.FIND_DEBUT;
                        }
                        else
                        {
                            state = State.EVAL_DEBUT;
                            idxTokenStartLine = idxLine;
                            idxTokenStartCol = idxColumn;
                            token = "" + item;
                        }
                        break;

                    case State.EVAL_DEBUT:
                        if (char.IsLetterOrDigit(item))
                        {
                            token += item;
                            state = State.EVAL_DEBUT;
                        }
                        else if (char.IsWhiteSpace(item) || '\n' == item)
                        {
                            if (token.Trim().ToLower() == "debut")
                            {
                                state = State.FIND_KEY_OR_FIN;
                                person = new Person();
                                token = "";
                            }
                            else
                            {
                                state = State.ERROR;
                                errorMessage = $"Debut expected but {token} found at line {idxTokenStartLine}:{idxTokenStartCol}";
                            }
                        }
                        else
                        {
                            state = State.ERROR;
                            errorMessage = $"Invalid character '{item}' at line {idxLine}:{idxColumn}";
                        }

                        break;

                    case State.FIND_KEY_OR_FIN:
                        if (char.IsLetter(item) || '_' == item)
                        {
                            idxTokenStartLine = idxLine;
                            idxTokenStartCol = idxColumn;
                            token = "" + item;
                            state = State.EVAL_KEY_OR_FIN;
                        }
                        else if (char.IsWhiteSpace(item) || '\n' == item)
                        {
                            state = State.FIND_KEY_OR_FIN;
                        }
                        else
                        {
                            state = State.ERROR;
                            errorMessage = $"Invalid character but key or fin expected at {idxLine}:{idxColumn}";
                        }
                        break;
                    case State.EVAL_KEY_OR_FIN:
                        if (char.IsLetterOrDigit(item))
                        {
                            token += item;
                            state = State.EVAL_KEY_OR_FIN;
                        }
                        else if (char.IsWhiteSpace(item) || '\n' == item)
                        {
                            if (token.ToLower() == "fin")
                            {
                                list.Add(person);
                                person = null;
                                state = State.FIND_DEBUT;
                            }
                            else
                            {
                                state = State.EVAL_KEY_OR_FIN;
                                if ('\n' == item)
                                {
                                    state = State.ERROR;
                                    errorMessage = $"Identifier {token} but no value given, ':' expected at line {idxLine}:{idxColumn}";
                                }
                            }
                        }
                        else if (':' == item)
                        {
                            field = token.Trim().ToLower();
                            string[] fields = { "nom", "prenom", "age" };
                            if (!fields.Contains(field))
                            {
                                state = State.ERROR;
                                errorMessage = $"Field {field} is not a valid identifier at line {idxTokenStartLine}:{idxTokenStartCol}" ;
                            }
                            else
                            {
                                state = State.FIND_VALUE;
                            }
                        }
                        break;

                    case State.FIND_VALUE:
                        if (char.IsWhiteSpace(item))
                        {
                            state = State.FIND_VALUE;
                        }
                        else if ('\n' == item)
                        {
                            state = State.FIND_VALUE;
                        }
                        else
                        {
                            idxTokenStartCol = idxColumn;
                            idxTokenStartLine = idxLine;
                            token = "" + item;
                            state = State.EVAL_VALUE;
                        }
                        break;

                    case State.EVAL_VALUE:
                        if ('\n' != item)
                        {
                            token += item;
                            state = State.EVAL_VALUE;
                        }
                        else
                        {
                            state = State.FIND_KEY_OR_FIN;
                            if (field == "nom") person.Lastname = token.Trim();
                            if (field == "prenom") person.Firstname = token.Trim();
                            if (field == "age")
                            {
                                try
                                {
                                    person.Age = int.Parse(token.Trim());
                                }
                                catch(FormatException ex)
                                {
                                    state = State.ERROR;
                                    errorMessage = $"At line{idxTokenStartLine}:{idxTokenStartCol} : {ex.Message}";
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
                if (null != AfterEvalItem) AfterEvalItem(this, new ParsingArgs()
                {
                    Item = item,
                    Token = token,
                    State = state,
                    Line = idxLine,
                    Column = idxColumn
                });

                if (state == State.ERROR)
                {
                    throw new Exception($"Parsing error : {errorMessage}");
                }

                idxColumn++;
                if (item == '\n')
                {
                    idxLine++;
                    idxColumn = 1;
                }
            }

            return list;
        }
    }
}
