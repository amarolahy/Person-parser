﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace PersonFileParser
{
    public partial class Form1 : Form
    {
        private String filePath;
        private BindingSource bs = new BindingSource();
        // Exemple azo ampiasaina
        /**
debut
  nom: john
  prenom: doe
fin

debut
  age: 16
  nom: foo
fin


debut
   prenom: complet
nom: info
      age   :   50
fin
debut
nom: nom
fin

debut
    prenom: samy
fin

debut
   age: 25
fin
        **/

        public Form1()
        {
            InitializeComponent();
            openFileDialog1.FileOk += onOpenFile;
            this.filePath = "";
            this.lblStatus.Text = "Status";
            string initialDirectory = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName;
            openFileDialog1.FileName = "persons.txt";
            openFileDialog1.InitialDirectory = initialDirectory;
            txtFilePath.Text = openFileDialog1.FileName;
            txtFileContent.Text = File.ReadAllText($"{initialDirectory}/{openFileDialog1.FileName}");
            dgvListPersons.DataSource = bs;
        }

        private void btnSearchFile_Click(object sender, EventArgs e)
        {
            var tmp = openFileDialog1.FileName.Split('\\');
            openFileDialog1.FileName = tmp[tmp.Length - 1];
            openFileDialog1.ShowDialog();
        }

        private void onOpenFile(object sender, CancelEventArgs e)
        {
            this.txtFilePath.Text = openFileDialog1.FileName;
            String fileContent = File.ReadAllText(openFileDialog1.FileName);
            txtFileContent.Text = fileContent;
        }

        private void btnParseFile_Click(object sender, EventArgs e)
        {
            IParser<Person> parser = new PersonParser();
            try
            {
                lblStatus.Text = "Begin parsing";
                bs.DataSource = parser.Parse(txtFileContent.Text);
                lblStatus.Text = "Code parsed successfully";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                lblStatus.Text = ex.Message;
            }
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            var tmp = openFileDialog1.FileName.Split('\\');
            openFileDialog1.FileName = tmp[tmp.Length - 1];
            openFileDialog1.ShowDialog();
        }
    }
}
